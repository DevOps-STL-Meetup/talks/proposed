# Proposed

This repo is designed to allow members of the community to submit talks for the Devops Meetup STL.  

Please create a Pull Request with the following information:  
Your Name  
Talk Name  
Date you'd like to deliver the talk on  